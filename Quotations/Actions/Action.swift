enum Action {
    case updateTicks([Quotation])
    case subscribe(CurrencyPair)
    case unsubscribe(CurrencyPair)
    case changeOrder(OrderChange)
}

struct OrderChange {
    let from: Int
    let to: Int
}
