import Foundation
import Starscream

extension CurrencyPair {
    func toSymbolString() -> String {
        return "\(self.firstSymbol)\(self.secondSymbol)"
    }
    
    static func arrayToSymbolString(_ array:[CurrencyPair]) -> String {
        return array.map({$0.toSymbolString()}).joined(separator: ",")
    }

    static func fromSymbolString(_ string: String) -> CurrencyPair? {
        switch string {
        case "EURUSD":
            return CurrencyPair(firstSymbol: .EUR, secondSymbol: .USD)
        case "EURGBP":
            return CurrencyPair(firstSymbol: .EUR, secondSymbol: .GBP)
        case "USDJPY":
            return CurrencyPair(firstSymbol: .USD, secondSymbol: .JPY)
        case "GBPUSD":
            return CurrencyPair(firstSymbol: .GBP, secondSymbol: .USD)
        case "USDCHF":
            return CurrencyPair(firstSymbol: .USD, secondSymbol: .CHF)
        case "USDCAD":
            return CurrencyPair(firstSymbol: .USD, secondSymbol: .CAD)
        case "AUDUSD":
            return CurrencyPair(firstSymbol: .AUD, secondSymbol: .USD)
        case "EURJPY":
            return CurrencyPair(firstSymbol: .EUR, secondSymbol: .JPY)
        case "EURCHF":
            return CurrencyPair(firstSymbol: .EUR, secondSymbol: .CHF)
        default:
            return nil
        }
    }
}

protocol QuotationsSource {
    func subscribe(pairs: [CurrencyPair])
    func unsubscribe(pairs: [CurrencyPair])
    var delegate: QuotationsSourceDelegate? {get set}
}

protocol QuotationsSourceDelegate {
    func quotesReceived(ticks: [Quotation])
}

class ConcreteQuotationSource: QuotationsSource, ExnessConnectionDelegate {
    var delegate: QuotationsSourceDelegate?
    var pendingSubscriptions = Set<CurrencyPair>()
    var pendingUnsubscriptions = Set<CurrencyPair>()
    let exnessConnection: ExnessConnection
    
    init(exnessConnection: ExnessConnection) {
        
        self.exnessConnection = exnessConnection
        self.exnessConnection.delegate = self
        
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (_) in
            self.update()
        }
    }
    
    func subscribe(pairs: [CurrencyPair]) {
        for pair in pairs {
            self.pendingUnsubscriptions.remove(pair)
            self.pendingSubscriptions.insert(pair)
        }
    }
    
    func unsubscribe(pairs: [CurrencyPair]) {
        for pair in pairs {
            self.pendingSubscriptions.remove(pair)
            self.pendingUnsubscriptions.insert(pair)
        }
    }
    
    func update() {
        self.exnessConnection.sendSubscriptionCommand(pairs: Array(self.pendingSubscriptions))
        self.exnessConnection.sendSubscriptionCommand(pairs: Array(self.pendingUnsubscriptions))
    }
    
    func subscriptionUpdated(ticks: [Quotation]) {
        for tick in ticks {
            self.pendingSubscriptions.remove(tick.pair)
        }
        self.delegate?.quotesReceived(ticks: ticks)
    }
    
    func quotesReceived(ticks: [Quotation]) {
        self.delegate?.quotesReceived(ticks: ticks)
    }
}

protocol ExnessConnectionDelegate {
    func subscriptionUpdated(ticks: [Quotation])
    func quotesReceived(ticks: [Quotation])
}

class ExnessConnection:WebSocketDelegate {
    var delegate: ExnessConnectionDelegate?
    var socket = WebSocket(url: URL(string: "wss://quotes.exness.com:18400")!)
    func startConnection() -> Void {
        socket.delegate = self
        socket.connect()
    }
    
    func sendSubscriptionCommand(pairs: [CurrencyPair]) {
        let command = "SUBSCRIBE: " + CurrencyPair.arrayToSymbolString(pairs)
        if socket.isConnected {
            socket.write(string: command)
        } else {
            startConnection()
        }
    }
    
    func sendUnsubscriptionCommand(pairs: [CurrencyPair]) {
        let command = "UNSUBSCRIBE: " + CurrencyPair.arrayToSymbolString(pairs)
        if socket.isConnected {
            socket.write(string: command)
        } else {
            startConnection()
        }
    }
    
    internal func websocketDidConnect(socket: WebSocketClient) {
//        socket.write(string: "SUBSCRIBE: EURUSD,EURGBP")
    }
    
    internal func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        
    }
    
    internal func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        if let response = ExnessResponse.fromJSONString(string: text) {
            switch response {
            case .Subscription(let quotes):
                self.delegate?.subscriptionUpdated(ticks: quotes)
            case .Tick(let quotes):
                self.delegate?.quotesReceived(ticks: quotes)
            }
        }
    }
    
    internal func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        
    }
}

enum ExnessResponse {
    case Subscription([Quotation])
    case Tick([Quotation])
    
    static func fromJSONString(string: String) -> ExnessResponse? {
        if let result = convertToDictionary(text: string) {
            if let subscribedList = (result["subscribed_list"] as? [String: Any]) {
                if let ticks = (subscribedList["ticks"] as? [[String: Any]]) {
                    if let q = ExnessResponse.quotationsFromArray(ticks: ticks) {
                        return ExnessResponse.Subscription(q)
                    }
                }
            }
            if let ticks = (result["ticks"] as? [[String: Any]]) {
                if let q = ExnessResponse.quotationsFromArray(ticks: ticks) {
                    return ExnessResponse.Tick(q)
                }
            }
        }
        
        return nil
    }
    
    static func quotationsFromArray(ticks: [[String: Any]]) -> [Quotation]? {
        let r = ticks.flatMap({(value: [String: Any]) -> Quotation? in
            if let quote = quotationFromDictionary(tick: value) {
                return quote
            }
            return nil
        })
        return r
    }
    
    static func quotationFromDictionary(tick: [String: Any]) -> Quotation? {
        let symbol = tick["s"] as! String
        let ask = tick["a"] as! String
        let bid = tick["b"] as! String
        let spread = tick["spr"] as! String
        if let pair = CurrencyPair.fromSymbolString(symbol) {
            return Quotation(pair: pair, abs: AskBidSpread(ask: ask, bid: bid, spread: spread))
        } else {
            return nil
        }
    }
    
    static func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}


