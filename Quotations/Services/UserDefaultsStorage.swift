import Foundation

final class UserDefaultsStorage: Storage {
    func save(_ state: State) {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(state), forKey:"state")
        UserDefaults.standard.synchronize()
    }
    
    func load() -> State? {
        if let data = UserDefaults.standard.object(forKey:"state") as? Data {
            if let state = try? PropertyListDecoder().decode(State.self, from: data) {
                return state
            }
        }
        return nil
    }
}
