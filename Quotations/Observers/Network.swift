import Foundation

final class Network: StateObserver, QuotationsSourceDelegate {
    
    var quotationSource: QuotationsSource
    let store: Store
    
    init(quotationSource: QuotationsSource, store: Store) {
        self.store = store
        self.quotationSource = quotationSource
        self.quotationSource.delegate = self
    }
    
    func newState(state: State) {
        let pairs = state.currencyPairList.flatMap({(value: SubscriptionType) -> CurrencyPair? in
            switch value {
            case .active(let pair):
                return pair
            default:
                return nil
            }
        })
        self.quotationSource.subscribe(pairs: pairs)
    }
    
    func quotesReceived(ticks: [Quotation]) {
        self.store.dispatch(action: Action.updateTicks(ticks))
    }
}
