import Foundation

protocol Storage {
    func save(_ state: State)
    func load() -> State?
}

final class Persistance: StateObserver {
    let storage: Storage
    
    init(storage: Storage) {
        self.storage = storage
    }
    
    func newState(state: State) {
        self.storage.save(state)
    }
}



