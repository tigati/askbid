import Foundation

final class UI: StateObserver {
    
    let navigationController: BaseNavigationController
    let store: Store
    
    init(navigationController:BaseNavigationController, store:Store) {
        self.navigationController = navigationController
        self.store = store
    }
    
    func newState(state: State) {
        let props = BaseNavigationController.Props(quotationVCProps: stateToQuotationVCProps(state: state), settingsVCProps: stateToSettingsVCProps(state: state))
        DispatchQueue.main.async {
            self.navigationController.props = props
        }
    }
    
    func stateToQuotationVCProps(state: State) -> QuotationsViewController.Props {
        var props: QuotationsViewController.Props
        
        let quotationList = state.quotationList
        
        func viewSettings(_ settingsVC: SettingsViewController) {
            let state = self.store.getState()
            let props = self.stateToSettingsVCProps(state: state)
            settingsVC.props = props
        }
        let onViewSettings = viewSettings
        
        if quotationList.count == 0 {
            props = .empty(QuotationsViewController.Props.Empty())
        } else {
            let q: [QuotationsViewController.Props.Quotation] = state.quotationList.map()
            {
                let pair = $0.pair
                func unsubscribe() {
                    self.store.dispatch(action: Action.unsubscribe(pair))
                }
                
                return QuotationsViewController.Props.Quotation(pair: $0.pair.description, ask: $0.abs.ask, bid: $0.abs.bid, spread: $0.abs.spread, onDeletion: unsubscribe)
            }
            func reorder(_ from: Int, _ to: Int) {
                self.store.dispatch(action: Action.changeOrder(OrderChange(from: from, to: to)))
            }
            let onReorder = (q.count > 1) ? reorder : nil
            props = .filled(QuotationsViewController.Props.Filled(quotes: q, onReorder:onReorder))
        }
        return props
    }
    
    func stateToSettingsVCProps(state: State) -> SettingsViewController.Props {
        let symbols = state.currencyPairList.map({(value: SubscriptionType) -> SettingsViewController.Props.Symbol in
            switch value {
            case .active(let pair):
                func unsubscribe() {
                    self.store.dispatch(action: Action.unsubscribe(pair))
                }
                let selected = SettingsViewController.Props.Selected(title: pair.description, onDeselection:unsubscribe)
                return SettingsViewController.Props.Symbol.selected(selected)
            case .passive(let pair):
                func subscribe() {
                    self.store.dispatch(action: Action.subscribe(pair))
                }
                let deselected = SettingsViewController.Props.Deselected(title: pair.description, onSelection:subscribe)
                return SettingsViewController.Props.Symbol.deselected(deselected)
            }
        })
        
        let props = SettingsViewController.Props(symbols: symbols)
        
        return props
    }
}
