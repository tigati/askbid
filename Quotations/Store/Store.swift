import Foundation

typealias Reducer<State> = (State, Action) -> State

protocol Dispatcher {
    func dispatch(action: Action)
}

protocol StateObserver {
    func newState(state: State)
}

final class Store: Dispatcher {
    private let queue = DispatchQueue(label: "Store queue")
    private var state: State
    private var reducer: Reducer<State>
    private var subscribers = Array<StateObserver>()
    
    init(state: State, reducer: @escaping Reducer<State>) {
        self.state = state
        self.reducer = reducer
    }
    
    func getState() -> State {
        return state
    }
    
    func subscribe(_ observer: StateObserver) {
        queue.async {
            self.subscribers.append(observer)
            observer.newState(state: self.state)
        }
    }
    
    func dispatch(action: Action) {
        queue.async {
            self.state = self.reducer(self.state, action)
            self.subscribers.forEach { $0.newState(state: self.state) }
        }
    }
}
