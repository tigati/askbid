import Foundation

struct State: Codable {
    let quotationList: [Quotation]
    let currencyPairList: [SubscriptionType]
}

struct Quotation: Codable {
    let pair: CurrencyPair
    let abs: AskBidSpread
}

extension Quotation: Hashable {
    var hashValue: Int {
        return pair.hashValue
    }
    
    static func == (lhs: Quotation, rhs: Quotation) -> Bool {
        return lhs.pair == rhs.pair
    }
}

enum SubscriptionType: Codable {
    case active(CurrencyPair)
    case passive(CurrencyPair)
}

extension SubscriptionType {
    private enum CodingKeys: String, CodingKey {
        case active
        case passive
    }
    
    enum SubscriptionTypeCodingError: Error {
        case decoding(String)
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? values.decode(CurrencyPair.self, forKey: .active) {
            self = .active(value)
            return
        }
        if let value = try? values.decode(CurrencyPair.self, forKey: .passive) {
            self = .passive(value)
            return
        }
        throw SubscriptionTypeCodingError.decoding("Whoops! \(dump(values))")
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        switch self {
        case .active(let pair):
            try container.encode(pair, forKey: .active)
        case .passive(let pair):
            try container.encode(pair, forKey: .passive)
        }
    }}

struct CurrencyPair: Codable {
    let firstSymbol: Currency
    let secondSymbol: Currency
}

extension CurrencyPair: Hashable {
    var hashValue: Int {
        return firstSymbol.hashValue ^ secondSymbol.hashValue
    }
    
    static func == (lhs: CurrencyPair, rhs: CurrencyPair) -> Bool {
        return lhs.firstSymbol == rhs.firstSymbol && lhs.secondSymbol == rhs.secondSymbol
    }
}

enum Currency: Int, Codable {
    case USD, EUR, GBP, JPY, CHF, CAD, AUD
}

struct AskBidSpread: Codable {
    let ask: String
    let bid: String
    let spread: String
}

let EURUSD: CurrencyPair = CurrencyPair(firstSymbol: .EUR, secondSymbol: .USD)
let EURGBP: CurrencyPair = CurrencyPair(firstSymbol: .EUR, secondSymbol: .GBP)
let USDJPY: CurrencyPair = CurrencyPair(firstSymbol: .USD, secondSymbol: .JPY)
let GBPUSD: CurrencyPair = CurrencyPair(firstSymbol: .GBP, secondSymbol: .USD)
let USDCHF: CurrencyPair = CurrencyPair(firstSymbol: .USD, secondSymbol: .CHF)
let USDCAD: CurrencyPair = CurrencyPair(firstSymbol: .USD, secondSymbol: .CAD)
let AUDUSD: CurrencyPair = CurrencyPair(firstSymbol: .AUD, secondSymbol: .USD)
let EURJPY: CurrencyPair = CurrencyPair(firstSymbol: .EUR, secondSymbol: .JPY)
let EURCHF: CurrencyPair = CurrencyPair(firstSymbol: .EUR, secondSymbol: .CHF)


