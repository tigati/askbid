extension State: CustomStringConvertible {
    var description: String {
        let quotationList = self.quotationList.map { "\($0)" }.joined(separator: "\n")
        let currencyPairList = self.currencyPairList.map { "\($0)" }.joined(separator: "\n")
        return "State:\n\(quotationList)\n\(currencyPairList)\n================\n"
    }
}

extension CurrencyPair: CustomStringConvertible {
    var description: String {
        return "\(firstSymbol)/\(secondSymbol)"
    }
}

extension SubscriptionType: CustomStringConvertible {
    var description: String {
        switch self {
        case .active(let pair):
            return "\(pair) V"
        case .passive(let pair):
            return "\(pair)"
        }
    }
}

