func reduce(_ state: State, with action: Action) -> State {
    switch action {
    case .updateTicks(let ticks):
        let newQuotationsList = state.quotationList.map({(value: Quotation) -> Quotation in
            if let index = ticks.index(of: value) {
                return ticks[index]
            }
            return value
        })
        return State(quotationList: newQuotationsList, currencyPairList: state.currencyPairList)
    case .subscribe(let pair):
        let newCurrencyPairList = state.currencyPairList.map({(value: SubscriptionType) -> SubscriptionType in
            switch value {
            case .active(_):
                return value
            case .passive(let innerPair):
                return (pair == innerPair) ? SubscriptionType.active(pair) : value
            }
        })
        let newQuotation = Quotation(pair: pair, abs: AskBidSpread(ask: "", bid: "", spread: ""))
        var newQuotationsList = state.quotationList
        if (!state.quotationList.contains(newQuotation)) {
            newQuotationsList.append(newQuotation)
        }
        
        return State(quotationList: newQuotationsList, currencyPairList: newCurrencyPairList)
    case .unsubscribe(let pair):
        let newCurrencyPairList = state.currencyPairList.map({(value: SubscriptionType) -> SubscriptionType in
            switch value {
            case .active(let innerPair):
                if (pair == innerPair) {
                    return SubscriptionType.passive(pair)
                } else {
                    return value
                }
//                return (pair == innerPair) ? SubscriptionType.passive(pair) : value
            case .passive(_):
                return value
            }
        })
        let newQuotationsList = state.quotationList.filter() { $0.pair != pair }
        return State(quotationList: newQuotationsList, currencyPairList: newCurrencyPairList)
    case .changeOrder(let orderChange):
        var newQuotationList = state.quotationList
        newQuotationList.rearrange(from: orderChange.from, to: orderChange.to)
        
        return State(quotationList: newQuotationList, currencyPairList: state.currencyPairList)
    }
}

