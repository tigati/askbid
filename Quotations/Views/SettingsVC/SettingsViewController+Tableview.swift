import UIKit

extension SettingsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return props.symbols.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        let symbol = props.symbols[indexPath.row]
        switch symbol {
        case .selected(let info):
            cell?.accessoryType = .none
            info.onDeselection()
        case .deselected(let info):
            cell?.accessoryType = .checkmark
            info.onSelection()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrencyPairCell")!
        
        let symbol = props.symbols[indexPath.row]
        switch symbol {
        case .selected(let info):
            cell.accessoryType = .checkmark
            cell.textLabel?.text = info.title
        case .deselected(let info):
            cell.accessoryType = .none
            cell.textLabel?.text = info.title
        }
        return cell
    }

}
