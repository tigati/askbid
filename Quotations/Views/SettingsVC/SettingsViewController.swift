//
//  SettingsViewController.swift
//  quotations
//
//  Created by Timur Gayfulin on 18/01/2018.
//  Copyright © 2018 Timur Gayfulin. All rights reserved.
//

import UIKit

class SettingsViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    
    struct Props {
        let symbols: [Symbol]
        enum Symbol {
            case selected(Selected)
            case deselected(Deselected)
        }
        
        struct Selected {
            let title: String
            let onDeselection: OnDeselection
        }
        
        struct Deselected {
            let title: String
            let onSelection: OnSelection
        }
        
        typealias OnSelection = () -> Void
        typealias OnDeselection = () -> Void
        
        static let initial = Props(symbols: [])
    }
    
    var props: Props = .initial {
        didSet {
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    

}
