//
//  BaseViewController.swift
//  quotations
//
//  Created by Timur Gayfulin on 21/01/2018.
//  Copyright © 2018 Timur Gayfulin. All rights reserved.
//

import UIKit

protocol PropViewController {
    
}

class BaseViewController: UIViewController, PropViewController {

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let nav = self.navigationController as? BaseNavigationController {
            nav.registerPropViewController(self)
        }
        
        if let pvc = self.presentingViewController {
            if pvc.isKind(of: BaseNavigationController.self) {
                let nav = pvc as? BaseNavigationController
                nav?.registerPropViewController(self)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let nav = self.navigationController as? BaseNavigationController
        nav?.unregisterPropViewController(self)
    }
}

class BaseTableViewController: UITableViewController, PropViewController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let nav = self.navigationController as? BaseNavigationController {
            nav.registerPropViewController(self)
        }
        
        if let pvc = self.presentingViewController {
            if pvc.isKind(of: BaseNavigationController.self) {
                let nav = pvc as? BaseNavigationController
                nav?.registerPropViewController(self)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let nav = self.navigationController as? BaseNavigationController
        nav?.unregisterPropViewController(self)
    }
}
