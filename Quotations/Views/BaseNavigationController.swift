//
//  NavigationViewController.swift
//  quotations
//
//  Created by Timur Gayfulin on 18/01/2018.
//  Copyright © 2018 Timur Gayfulin. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController {
    struct Props {
        let quotationVCProps: QuotationsViewController.Props
        let settingsVCProps: SettingsViewController.Props
        
        static let initial = Props(quotationVCProps: QuotationsViewController.Props.initial, settingsVCProps: SettingsViewController.Props.initial)
    }
    private var registeredPropViewControllers: [UIViewController] = []
    
    var props: Props = .initial {
        didSet {
            for vc in self.registeredPropViewControllers {
                setProps(vc: vc, props: self.props)
            }
        }
    }
    
    func registerPropViewController(_ viewController: UIViewController) {
        if (self.registeredPropViewControllers.index(of: viewController) == nil) {
            self.registeredPropViewControllers.append(viewController)
            setProps(vc: viewController, props: self.props)
        }
    }
    
    func unregisterPropViewController(_ viewController: UIViewController) {
        if let index = self.registeredPropViewControllers.index(of: viewController) {
            self.registeredPropViewControllers.remove(at: index)
        }
    }
    
    func setProps(vc:UIViewController, props:Props) {
        if vc is QuotationsViewController {
            let concreteVC = vc as! QuotationsViewController
            concreteVC.props = self.props.quotationVCProps
        }
        if vc is SettingsViewController {
            let concreteVC = vc as! SettingsViewController
            concreteVC.props = self.props.settingsVCProps
        }
    }

}
