//
//  ViewController.swift
//  quotations
//
//  Created by Timur Gayfulin on 18/01/2018.
//  Copyright © 2018 Timur Gayfulin. All rights reserved.
//

import UIKit

class QuotationsViewController: BaseTableViewController {
    enum Props {
        case empty(Empty)
        case filled(Filled)
        
        struct Empty {
//            let onViewSettings: OnViewSettings?
        }
        
        struct Filled {
            let quotes: [Quotation]
            let onReorder: OnReorder?
//            let onViewSettings: OnViewSettings?
        }
        
        struct Quotation {
            let pair: String
            let ask: String
            let bid: String
            let spread: String
            
            let onDeletion: OnDeletion
        }
        
        typealias OnReorder = (_ from: Int, _ to: Int) -> Void
        typealias OnDeletion = () -> Void
        typealias OnViewSettings = (_ settingsVC:SettingsViewController) -> Void

        static let initial = Props.empty(Empty())
    }
    
    var animatingDeletion = false
    
    var props: Props = .initial {
        didSet {
            switch props {
            case .empty(_):
                self.navigationItem.leftBarButtonItem?.isEnabled = false
                self.setEditing(false, animated: true)
                tableView.reloadData()
                break
            case .filled(let filled):
                self.navigationItem.leftBarButtonItem?.isEnabled = true
                if (filled.quotes.count != tableView.numberOfRows(inSection:0)) {
                    if animatingDeletion {
                        animatingDeletion = false
                        let index = IndexSet.init(integer: 0)
                        tableView.reloadSections(index, with: UITableViewRowAnimation.fade)
                    } else {
                        tableView.reloadData()
                    }
                } else {
                    configureVisibleCells(tableView, animated: true)
                }
            }
        }
    }
    
    
    @IBAction func myUnwindAction(unwindSegue: UIStoryboardSegue) {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let color = self.navigationItem.leftBarButtonItem?.tintColor
        self.navigationItem.leftBarButtonItem = self.editButtonItem;
        self.navigationItem.leftBarButtonItem?.tintColor = color
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func viewWillLayoutSubviews() {
    }
    
    func configureVisibleCells(_ tableView: UITableView, animated: Bool) {
        if let indexPaths = tableView.indexPathsForVisibleRows {
            configureRows(tableView, at: indexPaths, animated: animated)
        }
    }
    
    func configureRows(_ tableView: UITableView, at indexPaths: [IndexPath], animated: Bool) {
        for indexPath in indexPaths {
            if let cell = tableView.cellForRow(at: indexPath) {
                configureCell(in: tableView, cell: cell, forRowAt: indexPath)
            }
        }
    }
    
    func configureCell(in tableView: UITableView, cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let configuredCell = cell as! QuotationCell
        switch self.props {
        case .filled(let filled):
            configuredCell.pairLabel.text = filled.quotes[indexPath.row].pair
            configuredCell.askLabel.text = filled.quotes[indexPath.row].ask
            configuredCell.bidLabel.text = filled.quotes[indexPath.row].bid
            configuredCell.spreadLabel.text = filled.quotes[indexPath.row].spread
        default: break
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "QuotationCell") as! QuotationCell
        configureCell(in: tableView, cell: cell, forRowAt: indexPath)
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        switch self.props {
        case .empty:
            return 1
        case .filled(_):
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.props {
        case .empty:
            return 0
        case .filled(let filled):
            return filled.quotes.count
        }
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "delete") { (action, indexPath) in
            switch self.props {
            case .filled(let filled):
                self.animatingDeletion = true
                filled.quotes[indexPath.row].onDeletion()
            default:
                break
            }
        }
        return [delete]
    }
    
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        switch self.props {
        case .filled(let filled):
            filled.onReorder?(sourceIndexPath.row, destinationIndexPath.row)
        default:
            break
        }
    }

}

