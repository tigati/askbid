//
//  QuotationCell.swift
//  quotations
//
//  Created by Timur Gayfulin on 20/01/2018.
//  Copyright © 2018 Timur Gayfulin. All rights reserved.
//

import UIKit

class QuotationCell: UITableViewCell {
    @IBOutlet weak var pairLabel: UILabel!
    @IBOutlet weak var askLabel: UILabel!
    @IBOutlet weak var bidLabel: UILabel!
    @IBOutlet weak var spreadLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
